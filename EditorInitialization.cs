﻿namespace Codefarts.Settings.UnityXmlDocSettings
{
    using Codefarts.CoreProjectCode;
    using Codefarts.CoreProjectsCode.Interfaces;
    using Codefarts.IoC;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    [InitializeOnLoad]
    public class EditorInitialization
    {
        static XmlDocSettings provider;

        /// <summary>
        /// Initializes static members of the <see cref="EditorInitialization"/> class.
        /// </summary>
        static EditorInitialization()
        {
            // attempt to get the setting file location from EditorPrefs
            var settingsFilename = EditorPrefs.GetString(CoreGlobalConstants.SettingsFileKey, null);

            // check if settings not found
            if (string.IsNullOrEmpty(settingsFilename))
            {
                // set default path is my documents folder
                //settingsFilename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                //settingsFilename = Path.Combine(settingsFilename, "CodefartsSettings.xml");

                settingsFilename = Application.dataPath + "/CodefartsSettings.xml";

                //// attempt to create initial settings file if not already present
                //if (!File.Exists(settingsFilename))
                //{
                //    // create initial settings file
                //    File.WriteAllText(settingsFilename, "<?xml version=\"1.0\"?>\r\n<settings>\r\n</settings>");
                //}

                // save in EditorPrefs
                EditorPrefs.SetString(CoreGlobalConstants.SettingsFileKey, settingsFilename);
            }

            // if no file exists report to user
            if (!File.Exists(settingsFilename))
            {
                //EditorUtility.DisplayDialog("Warning", "No settings file exists! Click \"Edit->Preferences\" and select \"Codefarts\" to setup a settings file.", "Ok");
                Debug.LogError("Warning: No settings file exists! Click \"Edit->Preferences\" and select \"Codefarts\" to setup a settings file.");
                return;
            }

            provider = new XmlDocSettings(settingsFilename);
            Container.Default.Register<ISettingsProvider>(() => provider);
        }
    }
}